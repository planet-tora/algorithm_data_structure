const { printArray, randomNumberArray } = require('./utils')
function quickSort(array){
    printArray(array)
    if(array.length < 2){
        return array
    }
    const pivotIndex = array.length - 1
    const pivot = array[pivotIndex]
    const left = []
    const right = []
    for (let i = 0; i < pivotIndex; i++){
        const current = array[i]
        current < pivot ? 
            left.push(current) :
            right.push(current)
    }
    const result = [...quickSort(left),pivot,...quickSort(right)]
    return result
}
module.exports = {
    run(){
        printArray(quickSort(randomNumberArray()))
    }
}
