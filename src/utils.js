function printArray(arr){
    console.log( arr.reduce((p,c)=>{return p === '' ? c : `${p} ${c}`},""))
}
function randomNumberArray(){
    const arr = [1,2,3,4,5,6,7,8,9,10]
    arr.sort(() => Math.random() - 0.5);
    return arr
}
module.exports = {
    printArray,
    randomNumberArray
}