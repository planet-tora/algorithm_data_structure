class Stack {
    constructor(){
        this.array = []
    }
    push(item){
        this.array.push(item)
    }
    pop(){
        return this.array.pop()
    }
    peek(){
        return this.array[this.array.length - 1]
    }
    get length(){
        return this.array.length
    }
    isEmpty(){
        return this.length === 0
    }
}

module.exports = {
    run:() => {
        const lowerBodyStack = new Stack()
        lowerBodyStack.push('パンツ')
        lowerBodyStack.push('靴下')
        lowerBodyStack.push('ズボン')
        lowerBodyStack.push('靴')
        lowerBodyStack.pop()
        console.log(lowerBodyStack.peek());
    }
}