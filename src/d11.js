const {
    printArray,
    randomNumberArray
} = require('./utils')
function bubbleSort(array){
    let swapped = false
    do {
        printArray(array)
        swapped = false
        array.forEach((item, index)=>{
            if(item > array[index + 1]){
                const temp = item
                array[index] = array[index + 1]
                array[index + 1] = temp
                swapped = true
            }
        })
    } while (swapped)
    return array
}

module.exports = {
    run(){
        const numbers = randomNumberArray()
        bubbleSort(numbers)
    },
    printArray
}
