class BinaryTreeNode {
    constructor(key){
        this.key = key
        this.left = null
        this.right = null
    }
    addLeft(key){
        const newNode = new BinaryTreeNode(key)
        this.left = newNode
        return newNode
    }
    addRight(key){
        const newNode = new BinaryTreeNode(key)
        this.right = newNode
        return newNode
    }
}
const TRAVERSALS = {
    IN_ORDER :(node, visitFn)=> {
        if(node !== null){
            TRAVERSALS.IN_ORDER(node.left, visitFn)
            visitFn(node)
            TRAVERSALS.IN_ORDER(node.right, visitFn)
        }
    },
    PRE_ORDER :(node, visitFn)=> {
        if(node !== null){
            visitFn(node)
            TRAVERSALS.PRE_ORDER(node.left, visitFn)
            TRAVERSALS.PRE_ORDER(node.right, visitFn)
        }
    },
    POST_ORDER :(node, visitFn)=> {
        if(node !== null){
            TRAVERSALS.POST_ORDER(node.left, visitFn)
            TRAVERSALS.POST_ORDER(node.right, visitFn)
            visitFn(node)
        }
    },
}
class BinaryTree {
    constructor(rootKey){
        this.root = new BinaryTreeNode(rootKey)
    }
    print(traversalType = 'IN_ORDER'){
        let result = ''
        const visit = node => {
            result += result.length === 0
            ? node.key
            : ` => ${node.key}`
        }
        const fn = TRAVERSALS[traversalType]
        fn(this.root, visit)
        return result
    }
}
module.exports = {
    run(){
        const tree = new BinaryTree('a')
        const b = tree.root.addLeft('b')
        const c = tree.root.addRight('c')
        const d = b.addLeft('d')
        const e = b.addRight('e')
        const h = d.addLeft('h')
        const i = d.addRight('i')
        const f = c.addLeft('f')
        const g = c.addRight('g')
        console.log(tree.print('POST_ORDER'))
    }
}
