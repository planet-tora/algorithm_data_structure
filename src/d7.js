// function call counter with get
class CalledCounterProxy {
    constructor(target){
        target.funcCalledCounter = {}
        /**
         * Record the times of function calls
         * @param {String} functionName 
         */
        target.called = (functionName) => {
            const count = target.funcCalledCounter[functionName]
            console.log(`${functionName} has been called for ${count} time${count === 1 ? '': 's'}`);
            return count
        }
        return new Proxy(target, {
            get(target, prop){
                if(typeof target[prop] === 'function'){
                    const count = target.funcCalledCounter[prop] ? 
                                   target.funcCalledCounter[prop] + 1 : 1
                    target.funcCalledCounter[prop] = count
                }
                return target[prop] || 0
            }
        })
    }
}
// implement input data validator with set
class NumericProxy{
    constructor(target){
        return new Proxy(target, {
            set(target, prop, val){
                if (prop.startsWith('num')) {
                    if (typeof val === 'number'){
                        target[prop] = val
                        return true        
                    }else if(!isNaN(val)){
                        target[prop] = parseFloat(String(val))
                    }
                    return false                    
                }
                target[prop] = val
                return true
            }
        })
    }
}
// implement input data validator with set
class NumberArrayProxy{
    constructor(target){
        return new Proxy(target, {
            set(target, prop, val){
                if (typeof val === 'number'){
                    target[prop] = val
                    return true        
                }else if(!isNaN(val)){
                    target[prop] = parseFloat(String(val))
                    return true
                }
                console.warn('不能向NumberArrayProxy代理的对象加入非数字元素');
                return false
            }
        })
    }
}
// implement private class scope control
class PrivateProxy{
    constructor(target){
        return new Proxy(target, {
            get(target, prop){
                if(prop.startsWith("_")) return undefined
                if(typeof target[prop] === 'function'){
                    return target[prop].bind(target)
                }
                return target[prop]
            },
            ownKeys(target){
                return Object.keys(target).filter(key => !key.startsWith('_')).concat(['changePrivate'])
            },
            getOwnPropertyDescriptor(target,prop){
                return {
                    enumerable:true,
                    configurable:true
                }
            }
        })
    }
}
class PrivateClass{
    #es7 = 'Right';
    constructor(){
        this._innerCount = 0
        this.name = 'Tiger'
        //You can use proxy instance instead of a class instance to activate proxy functions
        return new PrivateProxy(this)
    }
    changePrivate(){
        this.#es7 = 'OK'
        console.log('private property changed', this.#es7);
    }
}

module.exports = {
    run() {
        // const target = {
        //     launch(){
        //         console.log('launch');
        //     },
        //     like(){
        //         console.log('like');
        //     }
        // }
        // const proxyCalled = new CalledCounterProxy(target)        // proxyCalled.launch()
        // proxyCalled.launch()
        // proxyCalled.like()
        // proxyCalled.called('launch');
        // proxyCalled.called('like');
        // console.log(proxyCalled.xyz);

        // const proxyNumeric = new NumericProxy(target)
        // proxyNumeric.name = "Tiger"
        // proxyNumeric.numAge = "AAAA"
        // console.log(proxyNumeric);

        // const numberArray = []
        // const proxyArr = new NumberArrayProxy(numberArray)
        // proxyArr.push(1)
        // proxyArr.push(2)
        // proxyArr.push('3')
        // proxyArr.push('Ha')
        // console.log(numberArray);

        const privateObj = new PrivateClass()
        for (const key in privateObj) {
            console.log(key);
        }
        console.log(privateObj._innerCount);
        // privateObj.changePrivate()
    }
}

// implement private mechanism with get 

