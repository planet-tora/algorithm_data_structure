class TreeNode {
    constructor(key){
        this.key = key;
        this.children = []
    }
    addChild(childKey){
        const childNode = new TreeNode(childKey)
        this.children.push(childNode)
        return childNode
    }
}
class Tree {
    constructor(rootKey){
        this.root = new TreeNode(rootKey)        
    }
    print(){
        let result = ''
        function traverse(node, visitFn, depth){
            visitFn(node, depth)
            node.children.forEach((child) => {
                traverse(child, visitFn, depth + 1)
            })
        }
        function visit(node, depth) {
            let indent = ' '.repeat(depth * 2)
            result += `${indent}${node.key}\r\n` 
        }
        traverse(this.root, visit, 0)
        return result
    }
}
module.exports = {
    run(){
        const dom  = new Tree('html')
        const head = dom.root.addChild('head')
        const body = dom.root.addChild('body')
        const title = head.addChild('title - egghead')
        const header = body.addChild('header')
        const main = body.addChild('main')
        const footer = body.addChild('footer')
        const h1 = header.addChild('h1')
        const p = main.addChild('p - Learn about trees')
        const copyRight = footer.addChild(`Copyrighe ${new Date().getFullYear()}`)
        console.log(dom.print());

    },
    TreeNode
}
