const { printArray,
        randomNumberArray } = require('./utils')
function insertionSort(array){
    let i,j
    for(i = 1 ; i < array.length; i++){
        for(j = 0; j < i; j++){
            printArray(array)
            if(array[i] < array[j]){
                const [item] = array.splice(i, 1)
                array.splice(j, 0, item)
            }
        }
    }
    return array
}

module.exports = {
    run(){
        const numbers = randomNumberArray()
        insertionSort(numbers)
    }
}
