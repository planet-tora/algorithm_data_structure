/**
 * Graph
 */
const Queue = require('./d1').Queue
class Node {
    constructor(key){
        this.key = key
        this.neighbors = []
    }
    addNeighbor(node){
        this.neighbors.push(node)
    }
}
class Graph {
    constructor(directed = false){
        this.directed = directed
        this.nodes = []
        this.edges = []
    }
    addNode(key){
        this.nodes.push(new Node(key))
    }
    getNode(key){
        return this.nodes.find(n => n.key === key)
    }
    addEdge(node1Key, node2Key){
        const node1 = this.getNode(node1Key)
        const node2 = this.getNode(node2Key)
        node1.addNeighbor(node2)
        this.edges.push(`${node1Key}-${node2Key}`)
        if (!this.directed) {
            node2.addNeighbor(node1)
        }
    }
    print(){
        return this.nodes.map(({key, neighbors}) => {
            let result = key
            if (neighbors.length) {
                result += `==> ${neighbors.map(n => n.key).join(' ')}`
            }
            return result
        }).join('\n')
    }
    breadthFirstSearch(startNodeKey, visitFn){
        const startingNode = this.getNode(startNodeKey)
        const visited = this.nodes.reduce((acc,node) => {
            acc[node.key] = false
            return acc
        },{})
        const queue = new Queue()
        queue.enqueue(startingNode)
        while(!queue.isEmpty()){
            const currentNode = queue.dequeue()
            if (!visited[currentNode.key]) {
                visitFn(currentNode)
                visited[currentNode.key] = true
            }
            currentNode.neighbors.forEach(node => {
                if(!visited[node.key]){
                    queue.enqueue(node)
                }
            })
        }
    }
}

module.exports = {
    run:()=>{
        const graph = new Graph(true)
        const nodes = ['a', 'b', 'c', 'd', 'e', 'f']
        const edges = [
            ['a','b'],
            ['a','e'],
            ['a','f'],
            ['b','d'],
            ['b','e'],
            ['c','b'],
            ['d','c'],
            ['d','e'],
        ]
        nodes.forEach((node)=>{
            graph.addNode(node)
        })
        edges.forEach((edge)=>{
            graph.addEdge(...edge)
        })
        graph.breadthFirstSearch('a', (node)=>{
            console.log(node.key);
        })
        // console.log(graph.print());
    },
    Graph,
    Node
}