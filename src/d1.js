class Queue {
    constructor(){
        this.queue = []
    }
    enqueue(item){
        this.queue.unshift(item)
    }
    dequeue(){
        return this.queue.pop()
    }
    peek(){
       return this.queue[this.queue.length - 1]
    }
    get length(){
        return this.queue.length
    }
    isEmpty(){
        return this.queue.length === 0
    }
}

module.exports = {
    run:() => {
        const q = new Queue()
        console.log(q.isEmpty());
        q.enqueue('Have an egghead lesson')
        q.enqueue('Read book')
        q.enqueue('Write a blog')
        console.log(q.peek());
        q.dequeue()
        console.log(q.peek());
        q.dequeue()
        console.log(q.peek());
    },
    Queue
}