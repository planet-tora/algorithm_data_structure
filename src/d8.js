
class RangeProxy{
    constructor(target){
        return new Proxy(target,{
            has(target,prop){
                return prop >= target.start && prop <= target.end
            }
        })
    }
}
function delay(f,ms){
    return function(){
        setTimeout(() => f.apply(this, arguments), ms);
    }
}

module.exports = {
    run(){
        const range = {
            start:0,
            end:10
        }
        const rangeProxy = new RangeProxy(range)
        console.log(100 in rangeProxy);
    }
}