const Queue = require('./d1').Queue
class PriorityQueue extends Queue{
    constructor(){
        super()
        this.lowPQueue = new Queue();
        this.highPQueue = new Queue();
    }
    enqueue(item, isHighPriority = false){
        if(isHighPriority){
            this.highPQueue.enqueue(item)
        }else{
            this.lowPQueue.enqueue(item)
        }
    }
    dequeue(){
        if(!this.highPQueue.isEmpty()){
            return this.highPQueue.dequeue()
        }
        return this.lowPQueue.dequeue()
    }
    peek(){
        if(!this.highPQueue.isEmpty()){
            return this.highPQueue.peek()
        }
        return this.lowPQueue.peek()
    }
    get length(){
        return this.highPQueue.length + this.lowPQueue.length
    }
    isEmpty(){
        return this.length === 0
    }
}
module.exports = {
    run:() => {
        const q = new PriorityQueue()
        q.enqueue('A fix here')
        q.enqueue('A bug here')
        q.enqueue('A new feature')
        console.log(q.peek());
        q.enqueue('A Emergency task', true)
        console.log(q.peek());
    }
}