/**
 * Depth first search 
 */
const Queue = require('./d1').Queue
const {Graph,Node} = require('./d5')
class DepthSearchGraph extends Graph {
    depthFirstSearch(startingNodeKey, visit){
        const startingNode = this.getNode(startingNodeKey)
        const visited = this.nodes.reduce((acc,node)=>{
            acc[node.key] = false
            return acc
        }, {})
        function explore(node){
            if(visited[node.key]){
                return 
            }
            visit(node)
            visited[node.key] = true
            node.neighbors.forEach(node => explore(node))
        }
        explore(startingNode)
    }
}
module.exports = {
    run(){
        const graph = new DepthSearchGraph(true)
        const nodes = ['a', 'b', 'c', 'd', 'e', 'f']
        const edges = [
            ['a','b'],
            ['a','e'],
            ['a','f'],
            ['b','d'],
            ['b','e'],
            ['c','b'],
            ['d','c'],
            ['d','e'],
        ]    
        nodes.forEach((node)=>{
            graph.addNode(node)
        })
        edges.forEach((edge)=>{
            graph.addEdge(...edge)
        })
        graph.depthFirstSearch('a',(node)=>{
            console.log(node.key);
        })
    }
}