# the same usage implemented in shell script
target="0"
# iterate src folder & find the largest number of file
for filename in ./src/*.js; do
  if [[ "$filename" = ./src/d* ]]
  then 
    replace=""
    find="./src/d"
    result=${filename//$find/$replace}
    result=${result//".js"/$replace}
    if [[ $target -lt $result ]]
      then
        target=$result    
    fi
  fi
done
# set the largest number plus 1 as the new file name
newFileName=$(( target + 1 ))
newFilePath="./src/d$newFileName.js" 
# create file and write initial content in it
touch $newFilePath
echo "module.exports = {
    run(){
        console.log('Number $newFileName')
    }
}" >> $newFilePath
code $newFilePath
echo "require('./d$newFileName').run()" > ./src/index.js
